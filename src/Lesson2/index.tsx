import React from "react";
import CardListIte from "./CardListItem";
import style from "./CardList.module.scss";

class CardList extends React.Component {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <h1>Это мой первый компонент</h1>
        <div className={style.block}>
          Тело компонента
          <CardListIte title="Я первый пропс" />
        </div>
      </>
    );
  }
}

export default CardList;
